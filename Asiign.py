def main():
    print("<- Welcome to the calculator ->")
    a = int(input("Enter the first number :: "))
    b = int(input("Enter the second number :: "))
    w = add(a,b)
    x = sub(a,b)
    y = mul(a,b)
    z = div(a,b)
    sel(w,x,y,z)

"""Doing the processing"""
def add(a,b):
    return a + b
def sub(a,b):
    return a - b
def mul(a,b):
    return a * b
def div(a,b):
    return a / b

"""Selecting and showing the result"""
def sel(w,x,y,z):
    ch = str(input("Enter your mode of operation : '+', '-', '*', '/' :: "))
    if (ch == '+'):
        print("The additon of the two numbers are ::",w,"\n")
    elif (ch == '-'):
        print("The substraction of the two numbers are ::",x,"\n")
    elif (ch == '*'):
        print("The multiplied result is ::",y,"\n")
    elif (ch == '/'):
        print("The divided result is ::",z,"\n")
    else:
        print("please choose an appropriate operation","\n")
main()
